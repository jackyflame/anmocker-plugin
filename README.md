# anmocker-plugin

#### 介绍
plugin lib for anmocker

#### 软件架构
通过annotation注解以及gradle插件，自动将对应的java source文件内容进行替换，方便实现非侵入式mock部分逻辑代码，且不会影响正常的功能逻辑开发，避免不必要的mock代码扰乱业务逻辑


#### 安装教程

引入前需加入jitpack库


```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```


gradle引入地址： 

`implementation 'com.gitee.jackyflame:anmocker-plugin:1.0.0-alpha'`

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
