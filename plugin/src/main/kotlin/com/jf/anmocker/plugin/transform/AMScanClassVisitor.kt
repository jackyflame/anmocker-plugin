package com.jf.anmocker.plugin.transform

import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

/**
 * @Class: MyScanClassVisitor
 * @Description:
 * @author:
 * @Date: 2022/9/21
 */
class AMScanClassVisitor(nextVisitor : ClassVisitor) : ClassVisitor(Opcodes.ASM5, nextVisitor) {

    override fun visitMethod(
        access: Int,
        name: String?,
        descriptor: String?,
        signature: String?,
        exceptions: Array<out String>?
    ): MethodVisitor {
        return super.visitMethod(access, name, descriptor, signature, exceptions)
    }
}