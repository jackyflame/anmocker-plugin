package com.jf.anmocker.plugin.mockcore

import javassist.CtClass
import javassist.CtMethod

interface IMethodMockAble {

    fun modifyMethod(targetCls : CtClass, method : CtMethod, ano : MethodAnoEntity) : Boolean

    fun insertBefore(targetCls : CtClass, method : CtMethod, ano : MethodAnoEntity) : Boolean

    fun insertAfter(targetCls : CtClass, method : CtMethod, ano : MethodAnoEntity) : Boolean

    fun replace(targetCls : CtClass, method : CtMethod, ano : MethodAnoEntity) : Boolean

}