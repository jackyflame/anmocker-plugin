package com.jf.anmocker.plugin.injector

import com.jf.anmocker.plugin.mockcore.AMClassMocker
import com.jf.mocker.anotations.IClassMocker
import javassist.ClassPool
import javassist.CtClass
import org.gradle.api.Project
import java.io.File


/**
 * @Class: MyMockerInjector
 * @Description:
 * @author:
 * @Date: 2022/9/22
 */
class AMInjector(project : Project?) {

    private var mClassPool : ClassPool = ClassPool.getDefault()
    private val classMocker = AMClassMocker(mClassPool)
    private val classCollector = AMClassCollector(mClassPool)

    init {
        /**加入android.jar，不然找不到android相关的所有类*/
        //mClassPool.appendClassPath(project.android.bootClasspath[0].toString())
        //println("MyMockerInjector >>> ${project.path}")
    }

    fun getClassPool() : ClassPool{
        return mClassPool
    }

    fun collectClassFileFromDir(file: File?) {
        file?.let {
            classCollector.collectClassFileFromDir(it.absolutePath)
        }
    }

    fun collectClassFileFromDir(absolutePath: String?) {
        absolutePath?.let {
            classCollector.collectClassFileFromDir(it)
        }
    }

    fun collectClassFileFromJar(file: File?) {
        file?.let {
            classCollector.collectClassFileFromJar(it)
        }
    }

    fun injectCollectClasses(){
        for(item in classCollector.getAnnotationMap().entries){
            classCollector.getClassMap()[item.key]?.let {
                classMocker.injectClass(it, item.value)
            } ?: println("injectClass ${item.key} jump : class not found")
        }
    }
}

data class InjectEntity(val classRootPath: String, var ctClass: CtClass?)

data class AnoEntity(val classMocker : IClassMocker, var ctClass: CtClass)