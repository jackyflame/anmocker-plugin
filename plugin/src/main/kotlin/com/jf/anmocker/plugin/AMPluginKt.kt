package com.jf.anmocker.plugin

import com.android.build.gradle.AppExtension
import com.jf.anmocker.plugin.transform.AMTransformKt
import org.gradle.api.Plugin
import org.gradle.api.Project

class AMPluginKt : Plugin<Project> {

    override fun apply(project: Project) {
        println("Hello gradle plugin from AMPluginKt")
        //project.tasks.register("greeting",
        //    Action<Task?> {
        //        println("Greeting from MyPlugin task!")
        //    })
        val appExtension: AppExtension? = project.extensions.findByType<AppExtension>(
            AppExtension::class.java
        )
        appExtension?.registerTransform(AMTransformKt(project))
    }

}