package com.jf.anmocker.plugin;

import com.android.build.gradle.AppExtension;
import com.jf.anmocker.plugin.transform.MyTransform;

import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

/**
 * A simple 'hello world' plugin.
 */
public class MyPlugin implements Plugin<Project> {

    public void apply(Project project) {
        System.out.println("Hello gradle plugin from MyPlugin");
        //project.getTasks().register("greeting", new Action<Task>() {
        //    @Override
        //    public void execute(Task task) {
        //        System.out.println("Greeting from MyPlugin task!");
        //    }
        //});
        AppExtension appExtension = project.getExtensions().findByType(AppExtension.class);
        if(appExtension != null){
            appExtension.registerTransform(new MyTransform(project));
        }else{
            System.out.println("registerTransform MyTransform error: AppExtension is null");
        }
    }
}