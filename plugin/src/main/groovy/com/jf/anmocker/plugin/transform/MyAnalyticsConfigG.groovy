package com.jf.anmocker.plugin.transform;

/**
 * @Class: MyAnalyticsConfigG
 * @Description:
 * @author:
 * @Date: 2022/9/21
 */
public class MyAnalyticsConfigG {
    private final static List<MyAnalyticsMethodCellG> sInterfaceMethods = new ArrayList<>();

    static {
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.view.View\$OnClickListener",
                "onClick(Landroid/view/View;)V", 1, 1));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.content.DialogInterface\$OnClickListener",
                "onClick(Landroid/content/DialogInterface;I)V", 1, 2));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.content.DialogInterface\$OnMultiChoiceClickListener",
                "onClick(Landroid/content/DialogInterface;IZ)V", 1, 3));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.CompoundButton\$OnCheckedChangeListener",
                "onCheckedChanged(Landroid/widget/CompoundButton;Z)V", 1, 2));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.RatingBar\$OnRatingBarChangeListener",
                "onRatingChanged(Landroid/widget/RatingBar;FZ)V", 1, 1));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.SeekBar\$OnSeekBarChangeListener",
                "onStopTrackingTouch(Landroid/widget/SeekBar;)V", 1, 1));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.AdapterView\$OnItemSelectedListener",
                "onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V", 1, 3));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.TabHost\$OnTabChangeListener",
                "onTabChanged(Ljava/lang/String;)V", 1, 1));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.AdapterView\$OnItemClickListener",
                "onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V", 1, 3));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.ExpandableListView\$OnGroupClickListener",
                "onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z", 1, 3));
        sInterfaceMethods.add(new MyAnalyticsMethodCellG("android.widget.ExpandableListView\$OnChildClickListener",
                "onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z", 1, 4));

    }

    public static MyAnalyticsMethodCellG isMatched(Set<String> interfaceList, String methodDesc) {
        for (MyAnalyticsMethodCellG methodCell : sInterfaceMethods) {
            if (interfaceList.contains(methodCell.getInterfaces()) && methodCell.getMethodDesc().equals(methodDesc)) {
                return methodCell;
            }
        }
        return null;
    }
}
