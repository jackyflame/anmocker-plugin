package com.jf.anmocker.plugin

import com.android.build.api.instrumentation.FramesComputationMode
import com.android.build.api.instrumentation.InstrumentationScope
import com.android.build.api.variant.AndroidComponentsExtension
import com.android.build.gradle.AppExtension
import com.jf.anmocker.plugin.transform.MyTransformG
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * @Class: groovy
 * @Description:
 * @author:
 * @Date: 2022/9/21
 */
public class MyPluginG implements Plugin<Project>{
    @Override
    public void apply(Project project) {
        //say hello
        println("Hello from plugin 'com.jf.anmocker.plugin.MyPluginG")
        // Register a task
        project.tasks.register("greeting"){
            doLast {
                println("Hello from plugin 'MyPluginG' task 'greeting'")
            }
        }
        registerTransform(project)
    }

    private static void registerTransform(Project project){
        AppExtension appExtension = project.getExtensions().findByType(AppExtension.class);
        appExtension.registerTransform(new MyTransformG(project));
    }

    private static void registerTransformFactory(Project project){
        //注册TransformFactory
        AndroidComponentsExtension androidComponents = project.extensions.getByType(AndroidComponentsExtension::class.java)
        androidComponents.onVariants { variant ->
            variant.instrumentation.transformClassesWith(MyTransformFactory::class.java,
                    InstrumentationScope.PROJECT) {}
            variant.instrumentation.setAsmFramesComputationMode(
                    FramesComputationMode.COMPUTE_FRAMES_FOR_INSTRUMENTED_METHODS
            )
        }
    }

}
